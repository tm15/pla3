package ga.manuelgarciacr.tripmemories;

import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import java.util.UUID;

import ga.manuelgarciacr.tripmemories.ui.home.HomeFragment;

import static ga.manuelgarciacr.tripmemories.ui.detail.DetailFragment.ARG_TRIP_ID;
import static ga.manuelgarciacr.tripmemories.ui.home.HomeFragment.ARG_RECORDS_LOADED;

public class MainActivity extends AppCompatActivity implements HomeFragment.Callbacks {

    private AppBarConfiguration mAppBarConfiguration;
    private NavController mNavController;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_add_black_24dp));
        fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
        final Activity that = this;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavController navController = Navigation.findNavController(that, R.id.nav_host_fragment);
                navController.navigate(R.id.nav_addtrip);
            }
        });
         */

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_map, R.id.nav_about)
                .setDrawerLayout(drawer)
                .build();
        mNavController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, mNavController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, mNavController);
        Bundle args = new Bundle();
        args.putInt(ARG_RECORDS_LOADED, 0);
        mNavController.navigate(R.id.nav_home,new Bundle());
    }

    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(mNavController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
    /****************************************************************
     *
     *     Callbacks
     */

    @Override
    public void onTripSelected(UUID trip) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_TRIP_ID, trip);
        NavDestination id;
        if ((id = mNavController.getCurrentDestination()) != null &&
                id.getId() == R.id.nav_home)
            mNavController.navigate(R.id.action_nav_home_to_nav_detail, args);
    }
/*
    @Override
    public void onTripDeleted(UUID trip) {
        Navigation.findNavController(this, R.id.nav_host_fragment).navigate(R.id.nav_home);
    }
*/
    /*
     *
     *     Callbacks
     ****************************************************************/

    @Override
    public void onBackPressed() {
        /*
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                if (doubleBackToExitPressedOnce) {
                    getActivity().finishAffinity();
                }
                doubleBackToExitPressedOnce = true;
                Toast.makeText(getActivity(), R.string.torneu_a_premer, Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
        */

        NavDestination id;
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else if((id = mNavController.getCurrentDestination()) != null && id.getId() == R.id.nav_home) {
            if (doubleBackToExitPressedOnce) {
                this.finishAffinity();
            }
            doubleBackToExitPressedOnce = true;
            Toast.makeText(this, R.string.torneu_a_premer, Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
        }else{
            mNavController.navigate(R.id.nav_home);
        }
    }
}
