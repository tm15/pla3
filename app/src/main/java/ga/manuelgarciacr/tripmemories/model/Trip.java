package ga.manuelgarciacr.tripmemories.model;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.util.Date;
import java.util.UUID;

@Entity(tableName = "table_trips")
public class Trip {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    private UUID uuid;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "country")
    private String country;
    @ColumnInfo(name = "date")
    private Date date;

    public Trip(@org.jetbrains.annotations.NotNull String name, String country) {
        uuid = java.util.UUID.randomUUID();
        this.setName(name);
        this.setCountry(country);
        this.setDate(new Date());
        Log.e("ESTRANY", "NEWTRIP: " + uuid.toString());
    }

    @NonNull
    public UUID getUuid() {
        return uuid;
    }

    void setUuid(@NonNull java.util.UUID uuid) {
        this.uuid = uuid;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NotNull String name) {
        this.name = name.trim();
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(@NotNull String country) {
        this.country = country.trim();
    }

    @NonNull
    public Date getDate() {
        return date;
    }

    public void setDate(@NonNull Date date) {
        this.date = date;
    }

    @NonNull
    @Override
    public String toString() {
        return getDate().toString()+getName()+getCountry()+getUuid();
    }
}
