package ga.manuelgarciacr.tripmemories.model;

import android.os.Handler;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

import ga.manuelgarciacr.tripmemories.ui.home.HomeViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


@SuppressWarnings({"unused", "unchecked"})
public class TripRepository {
    /*
    private final TripDao mTripDao;
    private final LiveData<List<Trip>> mAllTrips;

    public TripRepository(Application application) {
        TripDatabase db = TripDatabase.getDatabase(application);
        mTripDao = db.tripDao();
        mAllTrips = mTripDao.getTrips();
    }

    public LiveData<List<Trip>>  getAllTrips() {
        return mAllTrips;
    }

    public LiveData<Trip>  getTrip(UUID uuid) {
        return mTripDao.getTrip(uuid);
    }

    private static class InsertAsyncTask extends AsyncTask<Trip, Void, Void> {

        private TripDao asyncTaskDao;

        InsertAsyncTask(TripDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Trip... params) {
            asyncTaskDao.insert(params[0]);
            return null;
        }
    }

    public void insert(@NotNull String name, @NotNull String country){
        InsertAsyncTask task = new InsertAsyncTask(mTripDao);
        task.execute(new Trip(name, country));
    }

    public void updateTrip(Trip trip) {
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> mTripDao.updateTrip(trip));
    }

    public void insertTrip(Trip trip) {
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> mTripDao.insert(trip));
    }

    public void deleteTrip(Trip trip) {
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> mTripDao.deleteTrip(trip));
    }
    */

    private final RestApiService apiService;
//    private MutableLiveData<List<Trip>> mutableLiveDataTripList = new MutableLiveData<>();
    private MutableLiveData<Trip> mutableLiveDataTrip = new MutableLiveData<>();
//    private MutableLiveData<Trip> mutableLiveDataTripInserted = new MutableLiveData<>();
//    private MutableLiveData<HomeViewModel.Request> mutableLiveDataRequestInserted = new MutableLiveData<>();

    private TripRepository() {
        apiService = RetrofitInstance.getApiService();
    }


    private static TripRepository mTripRepository;

    public synchronized static TripRepository getInstance() {
        if (mTripRepository == null)
            mTripRepository = new TripRepository();
        return mTripRepository;
    }

    //public LiveData<Trip> getTrip() {return mutableLiveDataTrip;}
    public void allTrips(HomeViewModel.Request request, LiveData<HomeViewModel.Request> liveData) {
        Call<ArrayList<Trip>> call = apiService.allTrips();
        call.enqueue(new Callback<ArrayList<Trip>>() {
            @Override
            public void onResponse(@NotNull Call<ArrayList<Trip>> call, @NotNull Response<ArrayList<Trip>> response) {
                new Handler().postDelayed(() -> ((MutableLiveData)liveData).setValue(request.getRequestResponse(1, response.body())), 4000);
            }
            @Override
            public void onFailure(@NotNull Call<ArrayList<Trip>> call, @NotNull Throwable t) {
                ((MutableLiveData)liveData).setValue(request.getRequestResponse(-1, null));
                Log.e("ERROR TRIPPING", "ALLTRIPS " + t.getMessage());
            }
        });
        ((MutableLiveData)liveData).setValue(request);
    }

    public void insertTrip(HomeViewModel.Request request, LiveData<HomeViewModel.Request> liveData) {
        Call<Trip> call = apiService.insertTrip((Trip)request.getT());
        call.enqueue(new Callback<Trip>() {
            @Override
            public void onResponse(@NotNull Call<Trip> call, @NotNull Response<Trip> response) {
                ((MutableLiveData)liveData).setValue(request.getRequestResponse(1, response.body()));
            }
            @Override
            public void onFailure(@NotNull Call<Trip> call, @NotNull Throwable t) {
                ((MutableLiveData)liveData).setValue(request.getRequestResponse(-1, null));
                Log.e("ERROR TRIPPING", "INSERTTRIP " + t.getMessage());
            }
        });
        ((MutableLiveData)liveData).setValue(request);
    }

    public void deleteTrip(HomeViewModel.Request request, LiveData<HomeViewModel.Request> liveData) {
        Call<Void> call = apiService.deleteTrip(((Trip)request.getT()).getUuid().toString());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NotNull Call<Void> call, @NotNull Response<Void> response) {
                ((MutableLiveData)liveData).setValue(request.getRequestResponse(1, null));
            }
            @Override
            public void onFailure(@NotNull Call<Void> call, @NotNull Throwable t) {
                ((MutableLiveData)liveData).setValue(request.getRequestResponse(-1, null));
                Log.e("ERROR TRIPPING", "DELETETRIP " + t.getMessage());
            }
        });
        ((MutableLiveData)liveData).setValue(request);
    }

    public MutableLiveData<Trip> getTripById(UUID uuid) {
        Call<Trip> call = apiService.getTripById(uuid.toString());
        call.enqueue(new Callback<Trip>() {
            @Override
            public void onResponse(@NotNull Call<Trip> call, @NotNull Response<Trip>
                    response) {
                Trip mTrip = response.body();
                mutableLiveDataTrip.setValue(mTrip);
            }
            @Override
            public void onFailure(@NotNull Call<Trip> call, @NotNull Throwable t) {
                Log.e("ERROR TRIPPING", "GETTRIPBYID " + Objects.requireNonNull(t.getMessage()));
            }
        });
        return mutableLiveDataTrip;
    }

    public void deleteTrip(UUID uuid) {
        Call<Void> call = apiService.deleteTrip(uuid.toString());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NotNull Call<Void> call, @NotNull Response<Void> response) {
            }
            @Override
            public void onFailure(@NotNull Call<Void> call, @NotNull Throwable t) {
                Log.e("ERROR TRIPPING", "DELETETRIP " + Objects.requireNonNull(t.getMessage()));
            }

        });
    }

}
