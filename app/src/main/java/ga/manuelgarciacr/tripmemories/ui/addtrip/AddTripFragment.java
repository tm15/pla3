package ga.manuelgarciacr.tripmemories.ui.addtrip;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

//import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

import ga.manuelgarciacr.tripmemories.R;

public class AddTripFragment extends Fragment{

    private AddTripViewModel mViewModel;
    private View mView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        /*
        // Hide the FAB.
        ((FloatingActionButton) Objects.requireNonNull(getActivity()).findViewById(R.id.fab)).hide();
*/
        return (mView = inflater.inflate(R.layout.add_trip_fragment, container, false));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mViewModel = ViewModelProviders.of(this).get(AddTripViewModel.class);

        final EditText mCountry = mView.findViewById(R.id.edtAddTripCountry),
                mName = mView.findViewById(R.id.edtAddTripName);
        final Button button = mView.findViewById(R.id.button_save);

        button.setOnClickListener(view -> {
            Boolean countryIsEmpty = TextUtils.isEmpty(mCountry.getText());
            Boolean nameIsEmpty = TextUtils.isEmpty(mName.getText());
            if (countryIsEmpty || nameIsEmpty) {
                Snackbar.make(view, R.string.empty_not_saved, Snackbar.LENGTH_LONG)
                        .show();
            } else {
                mViewModel.insert(mName.getText().toString(), mCountry.getText().toString());

                // Hide the keyboard
                InputMethodManager imm = (InputMethodManager) Objects.requireNonNull(getContext()).getSystemService(Context.INPUT_METHOD_SERVICE);
                Objects.requireNonNull(imm).hideSoftInputFromWindow(view.getWindowToken(), 0);

                // Return
                Objects.requireNonNull(getActivity()).onBackPressed();
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        /*
        // Show the FAB
        FloatingActionButton fab = Objects.requireNonNull(getActivity()).findViewById(R.id.fab);
        if(!fab.isShown())
            fab.show();

         */
    }
}
