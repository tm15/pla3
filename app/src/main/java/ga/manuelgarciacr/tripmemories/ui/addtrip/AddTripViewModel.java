package ga.manuelgarciacr.tripmemories.ui.addtrip;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import org.jetbrains.annotations.NotNull;

import ga.manuelgarciacr.tripmemories.model.TripRepository;

@SuppressWarnings("unused")
public class AddTripViewModel extends AndroidViewModel {

    public AddTripViewModel(@NonNull Application application) {
        super(application);
        TripRepository tripRepository = TripRepository.getInstance();
    }

    void insert(@NotNull String name, @NotNull String country){
        //tripRepository.insert(name, country);
    }
}
