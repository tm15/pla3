package ga.manuelgarciacr.tripmemories.ui.detail;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;

import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.UUID;

import ga.manuelgarciacr.tripmemories.R;
import ga.manuelgarciacr.tripmemories.model.Trip;
import ga.manuelgarciacr.tripmemories.ui.home.HomeViewModel;
import ga.manuelgarciacr.tripmemories.util.DatePickerFragment;

public class DetailFragment extends Fragment implements DatePickerFragment.Callbacks{
    public static final String ARG_TRIP_ID = "trip_id";
    private static final String ARG_DIALOG_FRAGMENT = "date_picker_fragment";
    private static final int REQUEST_DATE = 0;
    private HomeViewModel mViewModel;
    private View mView;
    private UUID mUuid;
    private Trip mTrip = null;
    private EditText mEdtTripCountry, mEdtTripName;
    private Button mBtnTripDate;
    private boolean mIsNew = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        mUuid = (UUID) getArguments().getSerializable(ARG_TRIP_ID);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return (mView = inflater.inflate(R.layout.detail_fragment, container, false));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mEdtTripCountry = mView.findViewById(R.id.edtTripCountry);
        mEdtTripName = mView.findViewById(R.id.edtTripName);
        mBtnTripDate = mView.findViewById(R.id.btnTripDate);
        mBtnTripDate.setOnClickListener(view -> {
            DatePickerFragment datePickerFragment = new DatePickerFragment().newInstace(mTrip.getDate());
            datePickerFragment.setTargetFragment(this, REQUEST_DATE);
            assert getFragmentManager() != null;
            datePickerFragment.show(getFragmentManager(), ARG_DIALOG_FRAGMENT);
        });

        mViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(HomeViewModel.class);
        mViewModel.tripLiveData.observe(getViewLifecycleOwner(), trip -> {
            mTrip = trip;
            if(mTrip == null) {
                return;
            }
            if (mTrip.getCountry().equals("") && mTrip.getName().equals(""))
                mIsNew = true;
            updateUI();
        });
        mViewModel.loadUUID(mUuid);

        AppCompatActivity activity = (AppCompatActivity) Objects.requireNonNull(getActivity());
        ActionBar actionBar = Objects.requireNonNull(activity.getSupportActionBar());
        actionBar.setSubtitle(mUuid.toString());
        actionBar.setDisplayHomeAsUpEnabled(false);

    }

    @Override
    public void onDateSelected(Date date) {
        mTrip.setDate(date);
        updateDate();
    }

    private void updateUI() {
        mEdtTripCountry.setText(mTrip.getCountry());
        mEdtTripName.setText(mTrip.getName());
        updateDate();
    }

    private void updateDate(){
        mBtnTripDate.setText(String.format(new Locale("es", "ES"), "%s %te/%2$tm/%2$tY", getString(R.string.data_del_viatge), mTrip.getDate()));
    }

    @Override
    public void onStop() {
        super.onStop();
        Boolean countryIsEmpty = TextUtils.isEmpty(mEdtTripCountry.getText());
        Boolean nameIsEmpty = TextUtils.isEmpty(mEdtTripName.getText());
        if (countryIsEmpty || nameIsEmpty) {
            if (mIsNew) {
                mViewModel.deleteTrip(mTrip);
                Snackbar.make(mView, "Trip not saved. Name or country is empty", Snackbar.LENGTH_LONG)
                        .show();
            }else{
                Snackbar.make(mView, "Trip not updated. Name or country isempty", Snackbar.LENGTH_LONG)
                        .show();
            }
            return;
        }
        mTrip.setCountry(mEdtTripCountry.getText().toString());
        mTrip.setName(mEdtTripName.getText().toString());
        mViewModel.updateTrip(mTrip);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppCompatActivity activity = (AppCompatActivity) Objects.requireNonNull(getActivity());
        Objects.requireNonNull(activity.getSupportActionBar()).setSubtitle("");
    }
}
