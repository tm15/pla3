package ga.manuelgarciacr.tripmemories.ui.home;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import ga.manuelgarciacr.tripmemories.R;
import ga.manuelgarciacr.tripmemories.model.Trip;



public class HomeFragment extends Fragment {
    public static final String ARG_RECORDS_LOADED = "records_loaded";
    private Callbacks mCallbacks = null;
    private View mView;
    private HomeViewModel mViewModel;
    private TripListAdapter mAdapter;
    private ProgressBar mPgb;
    private int mPgbCount = 0;
    private TextView mTxvPgbHome;
    private HashMap<UUID, Date> requestMap = new HashMap<>();

    @Override
    public void onInflate(@NonNull Context context, @NonNull AttributeSet attrs, @Nullable Bundle savedInstanceState) {
        super.onInflate(context, attrs, savedInstanceState);
        Log.e("ESTRANY", "ONINFLATE");
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        Log.e("ESTRANY", "ONCREATEVIEW");
        return mView = inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAdapter = new TripListAdapter(getContext());
        RecyclerView recyclerView = mView.findViewById(R.id.rcvHomeFragment);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(HomeViewModel.class);

        ItemTouchHelper itemTouchHelper = new
                ItemTouchHelper(new SwipeToDeleteCallback(mAdapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);

        mPgb = mView.findViewById(R.id.pgbHome);
        mTxvPgbHome = mView.findViewById((R.id.txvPgbHome));

        mViewModel.allTrips();
        requestObserver(mViewModel.getAllTrips(), "Loading...","Load trips failed", "setAdapter", null);

    }

    /****************************************************************
     *
     *     Callbacks
    */

    public interface Callbacks {
        void onTripSelected(UUID trip);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        mCallbacks = (Callbacks) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    /*
     *
     *     Callbacks
     ****************************************************************/

    /****************************************************************
     *
     *     Toolbar menu
     */

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NotNull Menu menu, @NotNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        item.setEnabled(false);
        if (item.getItemId() == R.id.new_trip) {
            Trip trip = new Trip("", "");
            UUID uuid = mViewModel.insertTrip(trip);
            requestObserver(mViewModel.getInsertResult(), "Inserting...", "Update/insert trip failed", "detail", uuid);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        requestObserver(mViewModel.getDeleteResult(), "Deleting...", "Canceled insert trip failed", "loadTrips", null);
        requestObserver(mViewModel.getUpdateResult(), "Saving...", "Update/insert trip failed", "loadTrips", null);
    }

    @SuppressWarnings("unchecked")
    private void requestObserver(LiveData<HomeViewModel.Request> liveData, String prompt, String errorMsg, String action, UUID uuid ){

        liveData.observe(getViewLifecycleOwner(), request-> {
            if(uuid != null && uuid != request.getUuid())
                return;
            if(request.getSt() == 0) {
                if(requestMap.containsKey(request.getUuid()))
                    return;
                requestMap.put(request.getUuid(), new Date());
                mPgbCount++;
                mTxvPgbHome.setText(prompt);
                //getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                //        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                mPgb.setVisibility(ProgressBar.VISIBLE);
                return;
            }
            Date d= requestMap.remove(request.getUuid());
            if(d == null)
                return;
            if(--mPgbCount <= 0) {
                mPgbCount = 0;
                mTxvPgbHome.setText("");
                //getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                mPgb.setVisibility(ProgressBar.INVISIBLE);
            }
            if(request.getSt() < 0) {
                Snackbar.make(mView, errorMsg, Snackbar.LENGTH_LONG)
                        .show();
                return;
            }
            switch (action){
                case "setAdapter":
                    mAdapter.setTrips((List<Trip>)request.getT());
                    break;
                case "loadTrips":
                    mViewModel.allTrips();
                    requestObserver(mViewModel.getAllTrips(), "Loading...", "Load trips failed", "setAdapter", null);
                    break;
                case "detail":
                    mCallbacks.onTripSelected(((Trip)request.getT()).getUuid());
                    break;
            }
        });
    }

    public class TripListAdapter extends RecyclerView.Adapter<TripListAdapter.TripViewHolder>{
        private final LayoutInflater mInflater;
        private List<Trip> mAllTrips;
        private Trip mRecentlyDeletedItem;
        private int mRecentlyDeletedItemPosition;

        class TripViewHolder extends RecyclerView.ViewHolder{
            private final TextView tripName;

            TripViewHolder(@NonNull View itemView) {
                super(itemView);
                tripName = itemView.findViewById(R.id.textView);
            }
        }

        TripListAdapter(Context context) {
            mInflater = LayoutInflater.from(context);
        }

        void setTrips(List<Trip> trips) {
            mAllTrips = trips;
            notifyDataSetChanged();
        }

        @NonNull
        @Override
        public TripListAdapter.TripViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = mInflater.inflate(R.layout.fragment_home_rcv_item, parent, false);
            return new TripListAdapter.TripViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull TripListAdapter.TripViewHolder holder, int position) {
            final Trip current = mAllTrips.get(position);
            holder.tripName.setText(current.getName());
            holder.tripName.setOnClickListener(v -> mCallbacks.onTripSelected(current.getUuid()));
        }

        @Override
        public int getItemCount() {
            if (mAllTrips != null)
                return mAllTrips.size();
            else return 0;
        }

        void deleteItem(int position) {
            mRecentlyDeletedItem = mAllTrips.get(position);
            mRecentlyDeletedItemPosition = position;
            mAllTrips.remove(position);
            mViewModel.deleteTrip(mRecentlyDeletedItem);
            notifyItemRemoved(position);
            showUndoSnackbar();
        }

        private void undoDelete() {
            mAllTrips.add(mRecentlyDeletedItemPosition,
                    mRecentlyDeletedItem);
            UUID uuid = mViewModel.insertTrip(mRecentlyDeletedItem);
            requestObserver(mViewModel.getInsertResult(), "Inserting...", "Update/insert trip failed", "loadTrips", uuid);
            notifyItemInserted(mRecentlyDeletedItemPosition);
        }

        private void showUndoSnackbar() {
            Snackbar snackbar = Snackbar.make(mView, R.string.snack_bar_text,
                    Snackbar.LENGTH_LONG);
            snackbar.setAction(R.string.snack_bar_undo, v -> undoDelete());
            snackbar.show();
        }

    }

    public class SwipeToDeleteCallback extends ItemTouchHelper.SimpleCallback {
        private TripListAdapter mAdapter;
        private Drawable icon;
        private final ColorDrawable background;

        SwipeToDeleteCallback(TripListAdapter adapter) {
            super(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
            mAdapter = adapter;
            icon = ContextCompat.getDrawable(Objects.requireNonNull(getContext()),
                    R.drawable.ic_delete_sweep_white_24dp);
            background = new ColorDrawable(Color.RED);
        }

        @Override
        public void onChildDraw(@NotNull Canvas c, @NotNull RecyclerView recyclerView, @NotNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            super.onChildDraw(c, Objects.requireNonNull(recyclerView), viewHolder, dX,
                    dY, actionState, isCurrentlyActive);
            View itemView = viewHolder.itemView;
            int backgroundCornerOffset = 0;

            // Only Background
            /*
            if (dX > 0) { // Swiping to the right
                background.setBounds(itemView.getLeft(), itemView.getTop(),
                        itemView.getLeft() + ((int) dX) + backgroundCornerOffset,
                        itemView.getBottom());

            } else if (dX < 0) { // Swiping to the left
                background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset,
                        itemView.getTop(), itemView.getRight(), itemView.getBottom());
            } else { // view is unSwiped
                background.setBounds(0, 0, 0, 0);
            }
            background.draw(c);
            */

            // Icon
            int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
            int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
            int iconBottom = iconTop + icon.getIntrinsicHeight();

            if (dX > 0) { // Swiping to the right
                int iconLeft = itemView.getLeft() + iconMargin;
                int iconRight = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                background.setBounds(itemView.getLeft(), itemView.getTop(),
                        itemView.getLeft() + ((int) dX) + backgroundCornerOffset,
                        itemView.getBottom());
            } else if (dX < 0) { // Swiping to the left
                int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                int iconRight = itemView.getRight() - iconMargin;
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset,
                        itemView.getTop(), itemView.getRight(), itemView.getBottom());
            } else { // view is unSwiped
                background.setBounds(0, 0, 0, 0);
            }

            background.draw(c);
            icon.draw(c);
        }

        /*
        @Override
        public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            if (mPgb.getVisibility() == ProgressBar.VISIBLE) return 0;
            return super.getSwipeDirs(recyclerView, viewHolder);
        }
        */

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            int position = viewHolder.getAdapterPosition();
            mAdapter.deleteItem(position);
        }
    }
}

