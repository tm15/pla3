package ga.manuelgarciacr.tripmemories.ui.home;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import java.util.UUID;

import ga.manuelgarciacr.tripmemories.model.Trip;
import ga.manuelgarciacr.tripmemories.model.TripRepository;

public class HomeViewModel extends AndroidViewModel {
    private final TripRepository mTripRepository;

    private LiveData<Request> mAllTrips = new MutableLiveData<>();
    private LiveData<Request> mInsertResult = new MutableLiveData<>();
    private LiveData<Request> mUpdateResult = new MutableLiveData<>();
    private LiveData<Request> mDeleteResult = new MutableLiveData<>();

    private MutableLiveData<UUID> tripIdLiveData = new MutableLiveData<>();
    public LiveData<Trip> tripLiveData;
    @SuppressWarnings({"unchecked", "PackageVisibleField", "unused"})
    public class Request<T>{
        private UUID uuid;
        private int st;
        private T t;
        private String aux;

        private Request (UUID uuid, int st, T t, String aux){
            super();
            if(uuid == null)
                uuid = UUID.randomUUID();
            setUuid(uuid);
            setSt(st);
            setT(t);
            setAux(aux);
        }

        private Request (T t, String aux){
            this(null, 0, t, aux);
        }

        private Request (T t){
            this(null, 0, t, "");
        }

        private Request (String aux){
            this(null, 0, null, aux);
        }

        private Request(){
            this(null, 0,null, "");
        }

        public Request getRequestResponse(int st, T t){
            return new Request(getUuid(), st, t, getAux());
        }

        public Request getRequestResponse(Integer st){
            return new Request(getUuid(), st, null, getAux());
        }

        public UUID getUuid() {
            return uuid;
        }

        private void setUuid(UUID uuid){
            this.uuid = uuid;
        }

        public Integer getSt() {
            return st;
        }

        private void setSt(Integer st) {
            this.st = st;
        }

        public T getT() {
            return t;
        }

        private void setT(T t) {
            this.t = t;
        }

        public String getAux() {
            return aux;
        }

        private void setAux(String aux) {
            this.aux = aux;
        }

        @NonNull
        @Override
        public String toString() {
            return getUuid().toString() + " : " + getSt() + " : " + getAux();
        }
    }



    public HomeViewModel(@NonNull Application application) {
        super(application);
        mTripRepository = TripRepository.getInstance();
        tripLiveData = Transformations.switchMap(tripIdLiveData, mTripRepository::getTripById);
    }

    public void allTrips() {
        mTripRepository.allTrips(new Request("list"), mAllTrips);
    }

    @SuppressWarnings("unchecked")
    public UUID insertTrip(Trip trip) {
        Request request = new Request(trip, "insert");
        mTripRepository.insertTrip(request, mInsertResult);
        return request.getUuid();
    }

    @SuppressWarnings("unchecked")
    public void updateTrip(Trip trip) {
        mTripRepository.insertTrip(new Request(trip, "update"), mUpdateResult);
    }

    @SuppressWarnings("unchecked")
    public void deleteTrip(Trip trip) {
        mTripRepository.deleteTrip(new Request(trip, "delete"), mDeleteResult);
    }

    public LiveData<Request> getAllTrips() {
        return mAllTrips;
    }

    public LiveData<Request> getInsertResult() {
        return mInsertResult;
    }

    public LiveData<Request> getUpdateResult() {
        return mUpdateResult;
    }

    public LiveData<Request> getDeleteResult() {
        return mDeleteResult;
    }

    public void loadUUID(UUID mTripId) {
        tripIdLiveData.setValue(mTripId);
    }

}